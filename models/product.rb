class Product < ActiveRecord::Base
	
	belongs_to :category
	belongs_to :brand
	
	has_many :product_properties
	has_many :line_items
	has_many :countries_vats

	validates :name, presence: true, uniqueness: true

	accepts_nested_attributes_for :countries_vats, :allow_destroy => true, reject_if: proc { |attributes| (attributes['country'].blank? || attributes['vat'].blank?) }

end
