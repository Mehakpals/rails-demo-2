class BankAccount < ActiveRecord::Base
	belongs_to :company
	has_many :to_bank_transactions,:foreign_key => "to_bank_account_id"
  	has_many :from_bank_transactions,:foreign_key => "from_bank_account_id"
	has_many :bank_account_members
	validates :account_id, uniqueness: :true, allow_nil: :true
end
