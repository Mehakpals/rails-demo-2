class Company < ActiveRecord::Base

    serialize :alias

    has_many :company_urls, class_name: 'CompanyUrl',:foreign_key => "spree_companies_id"
    has_many :logistics
    has_many :orders
    has_many :purchaser_orders, class_name: "Order", :foreign_key => "purchaser_id"
    has_many :seller_orders, class_name: "Order", :foreign_key => "seller_id"

    has_many :contacts, :as => :contactable
    has_many :price_lists, -> {with_deleted}, :as => :priceable
    has_many :bank_accounts

    has_many :bank_transactions

    has_many :company_addresses, :as => :addressable
    validates :name, presence: true
    has_many :companies_members
    has_many :companies_related_companies
    has_many :members, :through => :companies_members
    has_many :related_companies, :through => :companies_related_companies
    has_many :warehouse_related_companies, -> { where(:companies_related_companies => {relationship: 1}) }, through: :companies_related_companies,:source => "related_company"
    accepts_nested_attributes_for :company_urls, :allow_destroy => true
    accepts_nested_attributes_for :companies_related_companies, :allow_destroy => true, reject_if: proc { |attributes| (attributes['related_company_id'].blank?) }

    alias_attribute :address_line_1, :billing1
    alias_attribute :address_line_2, :billing2
    alias_attribute :postal_code, :zip_code
    alias_attribute :attention, :name

    after_create :generate_company_number

    attr_accessor :user_ids

    def generate_company_number
        num_val=(Company.maximum("company_id")||"0000").split("K")[1]
        self.update_column(:company_id,"K#{num_val.succ}")
    end

    def phone_number
      nil
    end

    def relationship
        relation = CompaniesRelatedCompany.find_by(related_company_id: id).relationship
        if relation == 1 
            "Warehouse Customer"
        elsif relation == 2
            "Parent" 
        elsif relation == 3
            "Supplier"
        end
    end

    def country_name
        if self.country.present?
            country_code = self.country
            country = ISO3166::Country[country_code]
            country.translations[I18n.locale.to_s] || country.name
        else
            self.try(:country)
        end    
    end
   
end
