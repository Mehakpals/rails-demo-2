class CompaniesController < ApplicationController
load_and_authorize_resource

  before_action :set_company, only: [:show, :edit, :update, :archive]
  before_action :set_company_address, only: [:adress_show, :edit_adress,:destroy]

  # GET /companies
  # GET /companies.json
  def index
    if params[:search_key].present?
    @companies = Company.where("name ILIKE ?","%#{params[:search_key]}%").accessible_by(current_ability).paginate(:page => (params[:page]||1),:per_page => 30).order("id ASC")
    else
    @companies = Company.accessible_by(current_ability).paginate(:page => (params[:page]||1),:per_page => 15).order("id ASC")
    end
  end

  # GET /companies/1
  # GET /companies/1.json
  def show
  end

  # GET /companies/new
  def new
    @company = Company.new
  end

  # GET /companies/1/edit
  def edit
  end

  # POST /companies
  # POST /companies.json
  def create
    @company = Company.new(company_params)
    respond_to do |format|
       # @member = current_member.id
      # if @company = @member.companies.new(contact_params)
      if @company.save
        
        format.html { redirect_to @company, notice: 'Company was successfully created.' }
        format.json { render :show, status: :created, location: @company }
      else
        format.html { render :new }
        format.json { render json: @company.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /companies/1
  # PATCH/PUT /companies/1.json
  def update
    respond_to do |format|
      if @company.update(company_params)
          if params[:company][:companies_related_companies_attributes].present?
            format.html { redirect_to "/companies/#{@company.id}/related_companies", notice: 'Company was successfully updated.' }
            flash[:notice] = 'Company was successfully related.'
            format.json { render :related_companies, status: :ok }
          else
            format.html { redirect_to @company, notice: 'Company was successfully updated.' }
            format.json { render :show, status: :ok, location: @company }
          end
      else
          if params[:company][:companies_related_companies_attributes].present?
            format.html { redirect_to "/companies/#{@company.id}/related_companies" }
            format.json { render json: @company.errors, status: :unprocessable_entity }
            flash[:error] = 'Company could not be related.'
          else
            format.html { render :edit }
            format.json { render json: @company.errors, status: :unprocessable_entity }
          end
      end
    end
  end

  # DELETE /companies/1
  # DELETE /companies/1.json
  def archive
    @company.destroy
    respond_to do |format|
      format.html { redirect_to companies_url, notice: 'Company was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def adress
    @company_adress = Company.find(params[:id]).company_addresses
  end

  def related_companies
    @related_companies = Company.find(params[:id]).related_companies
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_company
      @company = Company.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def company_params
        params.require(:company).permit(:company_type, :company_id, :name, :country, :url, :email,:billing1,
                                          :billing2,:city,:zip_code,:state,:as_customs_agent, 
                                          :as_shipping_agent, :telephone,user_ids:[],alias_names:[] ,
                                          company_urls_attributes: [
                                                                      :id, :url, :_destroy
                                                                      ], companies_related_companies_attributes:[:id,:related_company_id,:relationship])
    end
end
