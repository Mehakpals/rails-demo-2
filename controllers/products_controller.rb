class ProductsController < ApplicationController
  before_action :set_product, only: [:show, :edit, :update, :destroy]
  before_action :set_brand_category, only: [:show, :edit]
  

  # GET /products
  # GET /products.json
  def index
    @products = Product.all
  end

  # GET /products/1
  # GET /products/1.json
  def show
  end

  # GET /products/new
  def new
    @product = Product.new
    # @brand = Brand.all
    # @category = Category.all
  end

  # GET /products/1/edit
  def edit
  end

  # POST /products
  # POST /products.json
  def create
    @product = Product.new(product_params)
    respond_to do |format|
      if @product.save
        format.html { redirect_to @product, notice: 'Product was successfully created.' }
        format.json { render :show, status: :created, location: @product }
      else
        format.html { render :new }
        format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /products/1
  # PATCH/PUT /products/1.json
  def update
    respond_to do |format|
      if @product.update(product_params)
        format.html { redirect_to @product, notice: 'Product was successfully updated.' }
        format.json { render :show, status: :ok, location: @product }
      else
        format.html { render :edit }
        format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /products/1
  # DELETE /products/1.json
  def destroy
    @product.destroy
    respond_to do |format|
      format.html { redirect_to products_url, notice: 'Product was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def vat_via_ajax
     vat = Vat.find_by(country: params[:country])
     vat_result = []
     if vat.present?
       if vat.vat1.present?
          if params[:vat_search_key].present? 
             vat1 = vat.vat1.to_s
             if vat1.include?(params[:vat_search_key])
                vat_result << vat1.to_d
             end 
          else  
            vat_result << vat.vat1
          end  
       end
       if vat.vat2.present?
          if params[:vat_search_key].present? 
             vat2 = vat.vat2.to_s
             if vat2.include?(params[:vat_search_key])
                vat_result << vat2.to_d
             end 
          else
          vat_result << vat.vat2   
          end
       end
       if vat.vat3.present?
          if params[:vat_search_key].present? 
             vat3 = vat.vat3.to_s
             if vat3.include?(params[:vat_search_key])
                vat_result << vat3.to_d
             end 
          else
          vat_result << vat.vat3   
          end
       end
     end  
      respond_to do |format|
        format.html
        format.json { render :json => vat_result }
      end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_product
      @product = Product.find(params[:id])
    end

    def set_brand_category
      @product = Product.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def product_params
      params[:product].permit(:name, 
                              :product_type,
                              :category_id,
                              :brand_id, 
                              :category, 
                              :brand_article_number,
                              countries_vats_attributes: [:id,:country,:vat,:_destroy]
                              )
    end
end

              
